package com.vladromanchenko.pages;

import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.open;


/**
 * Created by v.romanchenko on 5/9/2016.
 */
public class GmailPage {

    public void vizit() {
        open("http://gmail.com");
    }

    public void login(String userName, String passwd) {
        $("#Email").setValue(userName).pressEnter();
        $("#Passwd").setValue(passwd).pressEnter();
    }

    public void refresh() {
        $(".asf").click();
    }
}
