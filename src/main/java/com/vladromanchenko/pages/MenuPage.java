package com.vladromanchenko.pages;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;

/**
 * Created by v.romanchenko on 5/11/2016.
 */
public class MenuPage {

    public void goToInbox() {
        $(byText("Inbox")).click();
    }

    public void goToSent() {
        $(byText("Sent Mail")).click();
    }
}
