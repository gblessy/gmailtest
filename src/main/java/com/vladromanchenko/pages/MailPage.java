package com.vladromanchenko.pages;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.CollectionCondition.exactTexts;
import static com.codeborne.selenide.CollectionCondition.texts;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selectors.by;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

/**
 * Created by v.romanchenko on 5/11/2016.
 */
public class MailPage {


    // public ElementsCollection mails = $$(".y6");
    public ElementsCollection mails = $$("[role=main] .zA");

    public void searchBySubject(String searchText) {
        $(by("name", "q")).setValue("subject:" + searchText).pressEnter();
    }

    public void assertMail(int index, String mailText) {
        mails.get(index).shouldHave(text(mailText));
    }

    public void assertMails(String... mailTexts) {
        mails.shouldHave(texts(mailTexts));
    }

    public void sendEmail(String address, String subject) {
        $(byText("COMPOSE")).click();
        $(by("name", "to")).setValue(address).pressEnter();
        $(by("name", "subjectbox")).setValue(subject).pressEnter();
        $(byText("Send")).click();
    }
}