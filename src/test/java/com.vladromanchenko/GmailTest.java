package com.vladromanchenko;

import com.codeborne.selenide.Configuration;
import com.vladromanchenko.pages.GmailPage;
import com.vladromanchenko.pages.MailPage;
import com.vladromanchenko.pages.MenuPage;
import org.junit.BeforeClass;
import org.junit.Test;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.open;
import static com.vladromanchenko.TestData.Config.password;
import static com.vladromanchenko.TestData.Config.userName;
import static java.lang.Math.random;


/**
 * Created by v.romanchenko on 5/9/2016.
 */
public class GmailTest {

    @BeforeClass
    public void setup(){
        Configuration.timeout = 15000;
    }

    GmailPage gmail = new GmailPage();
    MailPage mails = new MailPage();
    MenuPage menu = new MenuPage();


    @Test
    public void testLoginSendReceiveAndSearch() {

        String subject = "Test email" + random() * 10;

        gmail.vizit();

        gmail.login(userName, password);

        mails.sendEmail(userName, subject);

        gmail.refresh();
        mails.assertMail(0, subject);

        menu.goToSent();
        mails.assertMail(0, subject);

        menu.goToInbox();
        mails.searchBySubject(subject);
        mails.assertMails(subject);
    }
}